#include"winsock2.h"
#include<iostream>
#include<conio.h>
#include<stdio.h>

using namespace std;

#define MAX_PACKET_SIZE 1024
#define CONNECTION_PORT 8583


using namespace std;

int main(int argc, char* argv[])
{
    WSADATA wsaData;
    SOCKET sock;
    char message[MAX_PACKET_SIZE];
    int offset = 0;

    if(argc > 1 && argv[1][0] == 'r' && strlen(argv[1]) == 1)
    {
        WSAStartup(MAKEWORD(2,2), &wsaData);

        sock = socket(AF_INET,SOCK_DGRAM,0);

        char broadcast = '1';

        // This option is needed on the socket in order to be able to receive broadcast messages
        // If not set the receiver will not receive broadcast messages in the local network.
        if(setsockopt(sock,SOL_SOCKET,SO_BROADCAST,&broadcast,sizeof(broadcast)) < 0)
        {
            cout<<"Error in setting Broadcast option";
            closesocket(sock);
            return 0;
        }

        struct sockaddr_in Recv_addr;
        struct sockaddr_in Sender_addr;
        int len = sizeof(struct sockaddr_in);
        int recvbufflen = MAX_PACKET_SIZE;

        Recv_addr.sin_family       = AF_INET;
        Recv_addr.sin_port         = htons(CONNECTION_PORT);
        Recv_addr.sin_addr.s_addr  = INADDR_ANY;

        if (bind(sock,(sockaddr*)&Recv_addr, sizeof (Recv_addr)) < 0)
        {
            cout<<"Error in BINDING"<<WSAGetLastError();
            //_getch();
            closesocket(sock);
            return 0;
        }
        while(_getch() != 27){
            recvfrom(sock,message,recvbufflen,0,(sockaddr *)&Sender_addr,&len);
            cout<<"\n\n\tReceived: "<<message;
        }
        closesocket(sock);
        WSACleanup();

    }
    else if(argc > 2 && argv[1][0] == 's' && strlen(argv[1]) == 1)
    {
        // �������� ���������
		for(int i=3; i<argc && offset<MAX_PACKET_SIZE; i++)
		{
			strncpy(message+offset, argv[i], strlen(argv[i]));
			offset+=strlen(argv[i]);
			strcpy(message+offset, " ");
			offset++;
		}

        WSAStartup(MAKEWORD(2,2), &wsaData);
        sock = socket(AF_INET,SOCK_DGRAM,0);

        char broadcast = '1';

        if(setsockopt(sock,SOL_SOCKET,SO_BROADCAST,&broadcast,sizeof(broadcast)) < 0)
        {
            cout<<"Error in setting Broadcast option";
            closesocket(sock);
            return 0;
        }

        struct sockaddr_in Recv_addr;
        //struct sockaddr_in Sender_addr;

        //int len = sizeof(struct sockaddr_in);
        //char sendMSG[] ="Broadcast message from SLAVE TAG";

        //char recvbuff[MAX_PACKET_SIZE] = "";
        //int recvbufflen = MAX_PACKET_SIZE;

        Recv_addr.sin_family       = AF_INET;
        Recv_addr.sin_port         = htons(CONNECTION_PORT);
        Recv_addr.sin_addr.s_addr  = INADDR_BROADCAST; // this isq equiv to 255.255.255.255

        // better use subnet broadcast (for our subnet is 172.30.255.255)
        //Recv_addr.sin_addr.s_addr = inet_addr("255.255.255.255");

        sendto(sock,message,strlen(message)+1,0,(sockaddr *)&Recv_addr,sizeof(Recv_addr));

        closesocket(sock);
        WSACleanup();

    }
    else if(argc > 3 && argv[1][0] == 'p')
    {
        // TODO: ��������� ��������� ���� �� ������

    }
    else
    {
        printf("\nUsage: \n\"%s  r\" - receive message",argv[0]);
        printf("\n\"%s s some message\" - send some message to all", argv[0]);
        //printf("\n\"%s p ip some message\" - send some message to ip", argv[0]);
        //printf("\n\nExample: \"%s p 192.168.1.22 some message\" - send some message to ip 192.168.1.22",argv[0]);
        printf("\n\nExample: \"%s s some message\" - send some message to all",argv[0]);
        return 0;
    }

    return 0;
}
